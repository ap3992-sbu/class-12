package sbu.cs.real;

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class MongoConnection {

    MongoClient mongoClient;
    DB db;

    public MongoConnection(String database) throws UnknownHostException {
        mongoClient = new MongoClient();
        db = mongoClient.getDB(database);
    }

    public void insertToCollection(String collectionName, DBObject data) {
        DBCollection collection = db.getCollection(collectionName);
        collection.insert(data);
    }

    public List<Player> getPlayers(String collectionName) {
        DBCollection collection = db.getCollection(collectionName);
        DBCursor dbObjects = collection.find();
        List<Player> players = new ArrayList<>();
        for (DBObject dbObject : dbObjects) {
            players.add(Player.parsePlayer(dbObject));
        }
        return players;
    }

    public Player getPlayer(String collectionName, String username) {
        DBCollection collection = db.getCollection(collectionName);
        BasicDBObject obj = new BasicDBObject().append("username", username);
        DBObject one = collection.findOne(obj);
        return Player.parsePlayer(one);
    }
}
