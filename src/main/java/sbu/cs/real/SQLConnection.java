package sbu.cs.real;

import org.postgresql.util.PSQLException;

import java.io.Closeable;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SQLConnection implements Closeable {

    private static final String CONN_URL = "jdbc:postgresql://localhost:5432/test";
    private final Connection conn;
    private final String tableName;

    public SQLConnection(String tableName) throws SQLException, ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        this.conn = DriverManager.getConnection(CONN_URL, "ap", "ap");
        this.tableName = tableName;
        initiateTable();
    }

    private void initiateTable() throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (\n" +
                "  username varchar(45) NOT NULL,\n" +
                "  password varchar(65) NOT NULL,\n" +
                "  PRIMARY KEY (username)\n" +
                ")";
        Statement statement = conn.createStatement();
        statement.execute(sql);
        statement.close();
        System.out.println("initiate table done");
    }

    public void insertPlayer(Player player) throws SQLException {
        String sql = "INSERT INTO " + tableName + " VALUES (?, ?)";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, player.getUsername());
        ps.setString(2, player.getPassword());
        int i = 0;
        System.out.println(ps);
        try {
            i = ps.executeUpdate();
        } catch (PSQLException e) {
            System.out.println("can not insert to table " + e.getMessage());
        }
        ps.close();
        System.out.printf("%d rows effected %n", i);
    }

    public Player getPlayer(String username) throws SQLException {
        String sql = "select * from " + tableName + " where username = ?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        Player player = null;
        while (resultSet.next()) {
            player = Player.parsePlayer(resultSet);
        }
        statement.close();
        return player;
    }

    public List<Player> getPlayers() throws SQLException {
        String sql = "select * from " + tableName;
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(sql);
        List<Player> players = new ArrayList<>();
        while (resultSet.next()) {
            players.add(Player.parsePlayer(resultSet));
        }
        statement.close();
        return players;
    }

    @Override
    public void close() throws IOException {
        try {
            this.conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
