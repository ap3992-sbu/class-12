package sbu.cs.hint;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class App {

    public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
        String tableName = "player";
        SQLDatabaseConnection connection = new SQLDatabaseConnection(tableName);

        connection.addPlayer(tableName, "ali", "ali");
        connection.addPlayer(tableName, "morteza", "qwertyhn");
        connection.addPlayer(tableName, "sahar", "123");
        connection.addPlayer(tableName, "amir", "ali");

        List<Player> players = connection.getPlayers(tableName);
        for (Player player : players) {
            System.out.println(player);
        }

        System.out.println(connection.getPlayer(tableName, "ali"));
        System.out.println(connection.getPlayer(tableName, "amir"));

        connection.close();

        String database = "instagram";
        MongoDatabaseConnection mongo = new MongoDatabaseConnection(database);
        DBObject person = new BasicDBObject().append("username", "sahar").append("password", "pass");
        mongo.addToCollection(tableName, person);
        List<Player> collection = mongo.getCollection(tableName);
        for (Player player : collection) {
            System.out.println(player);
        }
    }
}
