package sbu.cs.hint;

import com.mongodb.*;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class MongoDatabaseConnection {

    private MongoClient mongoClient;
    private DB db;

    public MongoDatabaseConnection(String database) throws UnknownHostException {
            mongoClient = new MongoClient();
            db = mongoClient.getDB(database);
    }

    public void addToCollection(String collectionName, DBObject data) {
        DBCollection collection = db.getCollection(collectionName);
        collection.insert(data);
    }

    public List<Player> getCollection(String collectionName) {
        DBCollection collection = db.getCollection(collectionName);
        DBCursor dbObjects = collection.find();
        List<Player> players = new ArrayList<>();
        for (DBObject dbObject : dbObjects) {
            players.add(Player.parsePlayer(dbObject));
        }
        return players;
    }
}
